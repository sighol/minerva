\subsection{Observer}

To model the system we have chosen to use Euler angles to describe the rotations
	and the angular velocities.
We have used $\eta$ for the position and $\nu$ for velocities and $\tau$ for the
	thruster input.

To create an observer, first of all we have to know the system.
Given in the assignment text is the Rigid-Body-matrix $M_{RB}$, the
	added-mass-matrix $M_A$ and the linear-damping-matrix $D$.

The equation of motion for the rigid body can be described by the following
	equation.

\begin{align}
	\label{RGB}
	M\dot{\nu} + C_{RB}(\nu)\nu + C_A(\nu_r)\nu_r + D(\nu_r)\nu_r + g(\eta) +
		g_0 = \tau + \tau_{wind} + \tau_{wave}
\end{align}
where
\begin{align*}
	M = M_{RB} + M_A\\
	g(\eta) = 0\\
	\tau_{wind} = 0
\end{align*}

$\nu_r =[u\ v\ w\ p\ q\ r]^T$ is the relative velocity vector, where $[u\ v\ w]$
	are the linear velocities in each direction and $[p\ q\ r]$ are the angular
	velocities.
$\nu_c$ is the relative ocean current velocity vector.
The final velocity for the rigid body is

\begin{align}
	\nu = \nu_r + \nu_c
\end{align}

$g_0$ is the gravitational and bouncy forces that are acting on the ROV.
When no thrusters are active, the ROV will be forced up to the surface with 5N.

Since this force was so small, we decided to neglect its influence on the
	system in our observer.

The unknown coriolis-centripetal matrices $C_{RB}(\nu)$ and $C_A(\nu_r)$
	can be calculated from the given information.
$C_{RB}(\nu_r) $ and $ C_A(\nu_r)$ depend on the mass and added-mass and are
	calculated with the matlab-function \textit{m2c}.
To get the state-space-model we have to shift the equation of motion to

\begin{align}
	\dot{\nu} = M^{-1}(\tau - (C_A(\nu_r)\nu_r + C_{RB}(\nu)\nu + D(\nu_r)\nu_r)).
	\label{state}
\end{align}

The position of the vehicle is given in the vector

\begin{align*}
	\eta = [N \  E \  D \  \Phi \  \Theta \ \Psi ]^T
\end{align*}

and can be computed by the following formula

\begin{align*}
	\dot{\eta} = J(\eta) \nu.
\end{align*}

The $J$ matrix can be computed with the matlab function \textit{eulerang} in

Our state vector is

\begin{align}
	x &=
		\begin{bmatrix}
			\eta \\ \nu_r \\ \nu_c
		\end{bmatrix}
\end{align}

Since our model equation in \eqref{state} depends on two different velocity
	vectors, the state space had to mirror this as well.

The roll and pitch motion of the ROV is considered stable, so those from our
	state space to make the system simpler.

\subsubsection{Observer implementation}

	Since our system is nonlinear, we chose to implement it as such.
	We used matlab's symbolic toolbox to make this easier.

	The state vector $x$ and input vector $\tau$ is written as a vector of
		symbolic variables.
	Then we wrote down the equations of motion using the symbolic variables.

	\begin{align*}
		\dot{x} = f(x, \tau) = \begin{bmatrix}
				 M^{-1} (\tau - C_A \nu_r - C_{RB} (\nu_r +\nu_c) - D \nu_r) \\
				 J (\nu_r + \nu_c) \\
				 0
			\end{bmatrix}
	\end{align*}

	The A matrix can be computed using the \textit{jacobian} function.
	\begin{align*}
		A = \text{jacobian}(f)
	\end{align*}

	We then used the matlab function \textit{matlabFunction} to create functions
		returning the next value.
	The returned value would then be a vector or matrix of floats, so there
		should be no symbolic computations after this.

	We used Euler integration for the discretization.

	\begin{align}
		x_{k+1} = \text{ffun}(x_k, \tau_{k+1}) =
				x_k + dt \cdot f(x_k, \tau_{k+1})
	\end{align}

	We used the same method to get the linearized A matrix used in the Kalman
		Filter.

	\begin{align}
		A{k+1} = \text{phifun}(x_k) = I_{12x12} + dt \cdot A(x_k)
	\end{align}

	With these equations, in combination with the Kalman Filter, we should be
		able to accurately determine the values for $\nu_r$ and $\nu_c$.
	We could have added a fourth state $\nu_w$ to determine the influence of the
		waves.
	What we did was to consider the waves as noise on the output.

	The measured values can be transformed from the state matrix with
	\begin{align*}
		y &=
		\begin{bmatrix}
			N \\ E \\ D \\ r_r + r_c
		\end{bmatrix} =
		\begin{bmatrix}
			1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0\\
			0 & 1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0\\
			0 & 0 & 1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0\\
			0 & 0 & 0 & 1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0\\
			0 & 0 & 0 & 0 & 0 & 0 & 0 & 1 & 0 & 0 & 0 & 1
		\end{bmatrix}
		x \\
		&= H x.
	\end{align*}

	The final state space will then be

	\begin{align}
		x_{k+1} &= \text{ffun}(x_k, \tau_{k+1}) \\
		y_k &= H x_k
	\end{align}

	We tested the observability of the system using the $A$ matrix generated
		with $\eta$ set to all $0$.
	The rank of the observability matrix was the the same as the rank of the
		generated A matrix.
	This shows that the system is observable.

\subsubsection{EKF implementation}
	Because the system is nonlinear, we chose to use the discrete Extended
		Kalman Filter(EKF) for the observer.

	The EKF is implemented using a matlab class.
	This class is given some initial values along with the functions to
		calculate the next $x$ state (ffun) and the A matrix (phifun).

	Since our system is affected by noise, the state space becomes

	\begin{align}
		x_{k+1} &= \text{ffun}(x_k, \tau_{k+1}) \\
		y_k &= H x_k + v_k.
	\end{align}

	where $v_k$ is white noise.

	The implementation of the filters is just like in textbooks, except that we
		do the projection at the beginning of the cycle.
	The calculations goes as follows;

	At first, project ahead
	\begin{align*}
		A &= \text{phifun}(x) \\
		x^- &= \text{ffun}(x, \tau)\\
		P^- &= A P A^T + Q;
	\end{align*}

	and compute Kalman Gain
	\begin{align*}
		K = P^- H^T (H P^- H^T + R)^{-1}.
	\end{align*}

	Then update estimate with measurement
	\begin{align*}
		x = x^- + K (z - H x^-)
	\end{align*}

	and at last, compute error covariance for updated estimate
	\begin{align*}
		P &= (I - K H) P^- (I - K H)^T + K R K^T;
	\end{align*}

	\begin{comment}

		% Update estimate with measurement z_k
		x = x_ + K * (z - H * x_);

		% Compute error covariance for updated estimate
		ikh = I - K * H;
		P = ikh * P_ * ikh' + K * R * K';

		% Make sure P is symmetric
		P = (P + P') / 2;
	\end{comment}

The EKF estimates the states over the time.
For optimal precision and a fast result the $R$ and $Q$ matrix can chosen by the
	user.
The $R$ matrix describes the measured noise and the $Q$ matrix describes the
	system disturbance.
Hence you have to choose the $R$ and $Q$ so that only the states with noise and
	disturbance are influenced.
Both $R$ and $Q$ need to be positive definite.
For this model the noise and waves have not a influence on all states.
Therefore we choose

\begin{align}
    R = 	\begin{bmatrix}
		100 & 0    & 0     & 0     & 0\\
		0   & 100  & 0     & 0     & 0 \\
		0   & 0    & 100   & 0     & 0 \\
		0   & 0    & 0     & 1100 & 0\\
		0   & 0    & 0     & 0     & 1100
	\end{bmatrix}
    Q = I_{12x12}.
\end{align}

We chose $R$ like this of two reasons: first we want to get fast results.
Second we want to reduce the error on the NED position and filter the waves.
The NED position is calculated based on the estimated values for the velocity,
	while the velocity are compared directly with the measurement.
Therefore we believe that the velocity are more prone to noise than the
	position.

We chose $Q$ as the identity matrix.


