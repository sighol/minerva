\section{Theory}

In this part we will give a thorough documentation of the
	designs we have used to implement the observer and the thrust allocation.
This also includes all assumptions done during the implementation.

Information about the ROV model is also given.

\subsection{The ROV Minerva}

\begin{figure}[H]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.9\linewidth]{Pictures/minerva.jpg}
  \caption{The ROV Minerva}
  \label{fig:minerva}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \setlength{\tabcolsep}{18pt}
  \setlength{\extrarowheight}{5pt}
  \begin{tabular}{| l | c |}
    	\hline
    	\textbf{Length} & 1.44 [m]\\ \hline
    	\textbf{Width} & 0.82 [m]\\ \hline
    	\textbf{Height} & 0.80 [m]\\ \hline
    	\textbf{Mass} & 405 [m]\\
    	\hline
 	\end{tabular}
  \caption{Dimensions of the ROV}
  \label{fig:dimROV}
\end{subfigure}
\caption{The ROV Minerva and its dimensions}
\label{fig:ROVminerva}
\end{figure}

The roll and pitch motions are passively stable for the ROV, therefore we chose
	to neglect them in our control system to keep the system as simple as
	possible.

Rigid-body and added mass and linear damping are given by the following
	matrices:

\begin{align}
	M_{RB} =
	\begin{bmatrix}
		460 & 0 & 0 & 0 & 55.2 & 0 \\
		0 & 460 & 0 & -55.2 & 0 & 0 \\
		0 & 0 & 460 & 0 & 0 & 0 \\
		0 & -55.2 & 0 & 56.6 & 0 & 0 \\
		55.2 & 0 & 0 & 0 & 110.6 & 0 \\
		0 & 0 & 0 & 0 & 0 & 105
	\end{bmatrix}
\end{align}

\begin{align}
	M_{A} =
	\begin{bmatrix}
		290 & 0 & 0 & 0 & 0 & 0 \\
		0 & 300 & 0 & 0 & 0 & 0 \\
		0 & 0 & 330 & 0 & 0 & 0 \\
		0 & 0 & 0 & 110 & 0 & 0 \\
		0 & 0 & 0 & 0 & 56 & 0 \\
		0 & 0 & 0 & 0 & 0 & 55
	\end{bmatrix}
\end{align}

\begin{align}
	D_{L} =
	\begin{bmatrix}
		234 & 0 & 0 & 0 & 0 & 0 \\
		0 & 292 & 0 & 0 & 0 & 0 \\
		0 & 0 & 263 & 0 & 0 & 0 \\
		0 & 0 & 0 & 16 & 0 & 0 \\
		0 & 0 & 0 & 0 & 30 & 0 \\
		0 & 0 & 0 & 0 & 0 & 25
	\end{bmatrix}
\end{align}

ROV Minerva have in total five thrusters.
One lateral with propellers with diameter $D=0.19$ meters, and the other four
	have a diameter of $0.22$ meters.

\begin{figure}[H]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.9\linewidth]{Pictures/thrusters.png}
  \caption{Thruster positions}
  \label{fig:thrustpos}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \setlength{\tabcolsep}{18pt}
  \setlength{\extrarowheight}{5pt}
  \begin{tabular}{ l  c }
    	\hline
    	Entity & Value \\ \hline
    	$\alpha_{stb}$ & 10 [deg] \\
    	$\alpha_{port}$ & -10 [deg] \\
    	$x_{stb}$ & -0.57 [m] \\
    	$y_{stb}$ & 0.24 [m] \\
    	$x_{port}$ & -0.57 [m] \\
    	$y_{port}$ & -0.24 [m] \\
    	$x_{lat}$ & 0.166 [m] \\
    	\hline
 	\end{tabular}
  \caption{Thrusters geometry}
  \label{fig:valpos}
\end{subfigure}
\caption{The position and numbering system of the thrusters}
\label{fig:thrusters}
\end{figure}

\begin{figure}[H]
  \centering
  %\setlength{\tabcolsep}{18pt}
  %\setlength{\extrarowheight}{5pt}
  \begin{tabular}{ l  c }
    	\hline
    	Entity & Value \\ \hline
    	Surge Limits (fw bw) [N] & [-220 480] \\
    	Sway Limits (fw bw) [N] & [-220 220] \\
    	Heave Limits (fw bw) [N] & [-180 390] \\
    	\hline
 	\end{tabular}
  \caption{Thrusters limits}
  \label{fig:limits}
\end{figure}

\subsection{Thrust allocation}

	Since the DP system controls the force of the five thrusters of the ROV,
	but our control system give us forces in DOFs, we need to implement a thrust
		allocation.
	This block will contain the relation between the control vector $\tau$ and
		the thrust force, and it can therefore calculate the input $u$ to the
		controller.

The ROV thruster configuration satisfy
\begin{align}
	\tau = T(\alpha) u
\end{align}

Where the thrust configuration matrix for four degrees of freedom and five thrusters is

\begin{align}
	T(\alpha) =
	\begin{bmatrix}
	cos(\alpha_{stb}) & cos(\alpha_{port}) & 0 & 0 & 0\\
	sin(\alpha_{stb}) & sin(\alpha_{port}) & 1 & 0 & 0\\
	0 & 0 & 0 & 1 & 1\\
	-x_{stb} sin(\alpha_{stb}) - y_{stb} cos(\alpha_{stb}) & 
	x_{port} sin(\alpha_{port}) - y_{port} cos(\alpha_{port}) &
	x_{lat} & 0 & 0
	\end{bmatrix}
\end{align}

For the gain, $K$, we used the identity matrix.

Since we have no azimuth (rotatable) thrusters, the matrix is constant, and we
	have $T(\alpha)=T$.
This means that it is possible to find an \textit{optimal} distribution of the control
	forces for each DOF by using the Moore-Penrose pseudo-inverse,

\begin{align}
	T^+ = T^T (T T^T)^{-1}
\end{align}

And the control input vector $u$ can then be computed as

\begin{align}
	u = K^{-1} T^+ \tau
\end{align}

\subsubsection{Thrust saturation}

Prior to the thrust allocation we have implemented a saturation check for the
	thrust forces.
The saturation limits are given in table \eqref{fig:limits}.
The choice to do the saturation check prior to the thrust allocation was made so
	that no values exceeding the saturation limits can be fed into the observer.
The saturation check in Simulink is shown in figure \eqref{fig:saturation}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\linewidth]{Pictures/saturation.png}
	\caption{Saturation check in Simulink}
	\label{fig:saturation}
\end{figure}


\input{observer.tex}
