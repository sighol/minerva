function x = gensymvec(symbol, rows, cols)
    n = 1;
	for i = 1:rows
        for j = 1:cols
            t = sym([symbol, int2str(n)]);
            x(i,j) = t;
            n = n + 1;
        end
	end
end
