function [T, thrusterSaturation] = thrust_alloc()


thrusterSaturation.upper = [480; 220; 390; 100];
thrusterSaturation.lower = [-220; -220; -180; -100];
x_prt=-0.57;
x_stb=-0.57;
y_prt=-0.24;
y_stb=0.24;
alpha_prt=-10*pi/180;
alpha_stb=10*pi/180;
x_lat=0.166;

T_1 = [cos(alpha_stb); sin(alpha_stb); 0; x_stb*sin(alpha_stb) - y_stb*cos(alpha_stb)];
T_2 = [cos(alpha_prt); sin(alpha_prt); 0; -x_prt*sin(alpha_stb) - y_prt*cos(alpha_stb)];
T_3 = [0;1;0;x_lat];
T_4 = [0;0;1;0];
T_5 = [0;0;1;0];
T_thruster = [ T_1, T_2, T_3, T_4, T_5];

K_thruster_inv = eye(5);
T_thruster_inv = pinv(T_thruster);

config_thruster = K_thruster_inv * T_thruster_inv;
T = config_thruster;

end
