/*
* Generated S-function Target for model MINERVA. 
* This file provides access to the generated S-function target
* export file for other models.
*
* Created: Tue Feb 26 18:29:33 2013
*/

#ifndef RTWSFCN_MINERVA_sf_H
#define RTWSFCN_MINERVA_sf_H

#include "MINERVA_sfcn_rtw\MINERVA_sf.h"
  #include "MINERVA_sfcn_rtw\MINERVA_sf_private.h"

#endif
