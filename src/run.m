clc;
clear
clear globals;

addpath('model');

global filter

%% Initiating simulation timing
% This needs to be the same as the fixed time step of the simulink-model
dt = 0.02;
tstop = 1000;


%% Init
filter = ROV_Observer(dt);
[thruster.T, thruster.sat] = thrust_alloc();

%% Controller

Kp = diag([320 120 120 5]);
Ki =  diag([10 10 1 0]);
Kd = -diag([1200 1200 200 30]);

%% Initial Conditions
scenario = input('What scenario should be run? [1,2,3,4]: ');

zero = [0 0 200 0];
switch (scenario)
    case 1
        eta_r = [0 0 200 0];
        tstop = 240;
    case 2
        eta_r = [0 0 200 320*pi/180];
        tstop = 700;
    case 3
        eta_r = [100 60 150 320*pi/180];
    case 4
        zero = [0 0 1 0];
        eta_r = [10 0 5 0];
        tstop = 100;
end

eta_0 = to6dof(zero)';

tau = [100 0 0 0]';
filter.x_(1:4) = zero';


%% Initiating Model
model.current = 1;
model.waves = 1;
model.noise = 1;

%% Guidance
eta_diff = eta_r - zero;
omega = diag([1.5 0.7 1.2 0.3e-1]./abs(eta_diff));
for i = 1:length(eta_diff)
    if omega(i,i) > 1;
        omega(i,i) = 1;
    end
end
zetha = 1;
omega

%% Simulation

sim('Model2012a', tstop);

% Show angles in degrees
rads = [4 8];
x_hat(:,rads) = 180/pi * x_hat(:,rads);
y(:,4) = 180/pi * y(:,4);
y_hat(:,4) = 180/pi * y_hat(:,4);
guidance_out(:,4) = 180/pi * guidance_out(:,4);


%% Plotting setup



% Settings
p = struct();
p.trajectory = 1;
p.trajectory_animate = 0;
p.guidance = 0;
p.tau_sat = 1;
p.estimate =0;
p.bias = 0;

p.evolution = 1;
p.vel = 1;


%% Plotting
close all;

num = size(x_hat,1);
t = (0:num-1) * (tstop./num);

scen_str = sprintf('figs/sc-%d', scenario);

if p.trajectory
   f = figure();
   hold on;
   plot3(eta_r(2), eta_r(1), -eta_r(3), 'rX');
   plot3(eta_0(2), eta_0(1), -eta_0(3), 'X', 'color', 'black');
   plot3(y_hat(:,2), y_hat(:,1), -y_hat(:,3));
   grid on
   ylabel('North')
   xlabel('East')
   zlabel('Down')
   title('Trajectory for the ROV from the startpoint to the desired point')
   legend('Endpoint','Startpoint','Trajectory')
   axis equal

   saveas(f, [scen_str '-trajectory.pdf']);
end

if p.evolution
    f = figure;
    n = [y(:,1) y_hat(:,1) guidance_out(:,1)];
    e = [y(:,2) y_hat(:,2) guidance_out(:,2)];
    d = [y(:,3) y_hat(:,3) guidance_out(:,3)];
    psi = [y(:,4) y_hat(:,4) guidance_out(:,4)];
    
    subplot 411
    plot(t, n)
    title 'North'
    legend n n_h n_d
    
    subplot 412
    plot(t, e)
    legend e e_h e_d
    title East
    
    subplot 413
    plot(t, d)
    legend d d_h d_d
    title Down
    
    subplot 414
    plot(t, psi)
    legend psi psi_h psi_d
    title Yaw
    
    set(f, 'Position', [0 0 600 600]);
    saveas(f, [scen_str '-evolution.pdf']);
end

if p.vel
    f = figure;
   vel = x_hat(:,5:8);
   plot(t, vel);
   legend u v w r
   title 'Time evolution of the velocities'
   saveas(f, [scen_str '-vel-evolution.pdf']);
end

if p.trajectory_animate
   f = figure;
   hold on;
   plot(eta_r(2), eta_r(1), 'rX');
   plot(eta_0(2), eta_0(1), 'X', 'color', 'black');
   steps = 100;
   h = size(y,1)/steps;
   n_ = 1;
   for i = 1:steps;
       n = ceil(i*h);
       plot(y(n_:n,2), y(n_:n,1));
       pause(0.001);
       n_ = floor(n-1);
       if n_ < 1
           n_ = 1;
       end
   end
   saveas(f, [scen_str '-trajectory-animate.pdf']);
end

if p.guidance
   f = figure();
   y_g = [y(:,1) y(:,2) y(:,3) y(:,4)];

   subplot 211
   plot(t, [guidance_out y_g]);
   legend('g_x', 'g_y','g_z', 'g_yaw', 'x', 'y', 'z', 'yaw');
   title('Guidance vs estimates in NED');
   xlabel('time')
   ylabel('Value')

   subplot 212
   plot(t, [guidance_out-y_g]);
   legend x y z yaw
   title('Guidance - estimates in NED');
   xlabel('time')
   ylabel('Value')
   saveas(f, [scen_str '-guidance.pdf']);
end

if p.tau_sat
    f = figure;
    plot(t, tau_sat);
    title('tau saturized')
    legend tau_x tau_y tau_z tau_{psi}
    ylabel F[N]
    saveas(f, [scen_str '-tau.pdf']);
end

if p.estimate
    f = figure;
    subplot(2,1,1);
    plot(t, y-y_hat);
    legend y_1 y_2 y_3 y_4 y_5
    title('measured - estimated');

    subplot(2,1,2);
    plot(t, [y y_hat]);
    legend y_1 y_2 y_3 y_4 y_5 yh_1 yh_2 yh_3 yh_4 yh_5
    title('Measured And Estimate');

    saveas(f, [scen_str '-estimates.pdf']);

end

if p.bias
    f = figure;
    plot(t, x_hat(:,9:12));
    title 'Bias estimates'
    legend b1 b2 b3 b4
    xlabel t
    ylabel bias
    saveas(f, [scen_str '-bias.pdf']);
end

max_tau = max(tau_sat)

figure;
subplot 311, plot(t, tau_Kp)
subplot 312, plot(t, tau_Ki)
subplot 313, plot(t, tau_Kd)

figure;
plot(t, x_hat(:,9:12))
