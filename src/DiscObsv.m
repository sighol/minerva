function output = DiscObsv(input)
    % Inline matlab function for the EKF
    global filter;

    y = input(1:5);
    tau = input(6:9);

    filter.addMeasurement(y, tau);
    output = filter.get()';
end
