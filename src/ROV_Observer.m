function [filter] =  ROV_Observer(dt)
    %%Marine Control Project Part 1
    % Group 7 - Sigurd Holsen, Tor Kvestad, Guro Larsen
    % Obsverver design

    % Without the wave influence

    %% Parameters

    eta = symvec('eta', 4, 1);
    nu = symvec('nu', 4, 1);
    b = symvec('b', 4, 1);
    x = [eta; nu; b];
    tau = symvec('tau', 4, 1);

    xlen = length(x);

    %% Mass and Correolis and Damping Matrices

    M_RB = [460   0   0   0  55.2  0 ;
            0   460  0 -55.2  0   0 ;
            0    0  460  0    0   0 ;
            0  -55.2 0  55.6  0   0 ;
           55.2  0   0   0  110.6 0 ;
            0    0   0   0    0  105;];

    M_A  =  [290   0   0   0    0   0 ;
             0   300  0   0    0   0 ;
             0    0  330  0    0   0 ;
             0    0   0  110   0   0 ;
             0    0   0   0   56   0 ;
             0    0   0   0    0   55;];

    Damp  =  [234   0   0   0    0   0 ;
             0   292  0   0    0   0 ;
             0    0  263  0    0   0 ;
             0    0   0   16   0   0 ;
             0    0   0   0   30   0 ;
             0    0   0   0    0   25;];

    %% State-space Design
    small = [1 2 3 6];
    v_6DOF = to6dof(nu);

    M = M_RB+M_A; % Combined Mass and Added mass

    C_RB = m2c(M_RB,v_6DOF);
    C_A = m2c(M_A, v_6DOF);

    % Cropping the 6x6 matrices to the smaller 4x4 system
    M = M(small,small);
    C_A = C_A(small, small);
    C_RB = C_RB(small, small);
    Damp = Damp(small, small);

    %% Bias
    f_bdot = zeros(4,1);

    %% nudot
    J = eulerang(0, 0, eta(4));
    J = J(small,small);

    f_nudot = M \ (tau - (C_A + C_RB + Damp) * nu);
    f_etadot = J * nu;
    E = [eye(4);
        zeros(4)
        eye(4)];


    f = [f_etadot;
        f_nudot;
        f_bdot];
    F = jacobian(f, x);

    % phifun - returnes updated A-matrix
    getphicont = matlabFunction(F, 'vars', x, 'file', 'getphicont.m');
    phifun = @(x) eye(xlen) + dt * getphicont(...
        x(1), x(2), x(3), x(4), x(5), x(6), ...
        x(7), x(8), x(9), x(10), x(11), x(12));

    % ffun - returnes update state
    getfcont = matlabFunction(f, 'vars', [x; tau], 'file', 'getfcont.m');
    ffun = @(x, u) x + dt * getfcont(...
        x(1), x(2), x(3), x(4), x(5), x(6), x(7), ...
        x(8), x(9), x(10), x(11), x(12), u(1), u(2), u(3), u(4));

    H = [eye(4) zeros(4, 8);
         0 0 0 0   0 0 0 1 0 0 0 0];

    Q = diag([1 1 1 1]);

    filter = EKF();
    filter.ffun = ffun;
    filter.phifun = phifun;
    filter.x_ = zeros(xlen,1);
    filter.P_ = eye(length(x));
    filter.H = H;
    filter.Q = E * Q * E';
    filter.R = diag([100 100 100 10 10]);

    % Testing observability
    x = zeros(xlen,1);
    A = phifun(x);
    observability = obsv(A, H);
    if rank(observability) == rank(A)
        disp('Observability: The system is observable for x=0');
    end
end

