function [ v_6DOF ] = to6dof( v )

    v_6DOF = [v(1); v(2); v(3); 0; 0; v(4)];
end
