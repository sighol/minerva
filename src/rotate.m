function [ out ] = rotate(vec, vec_current)
    %ROTATE Summary of this function goes here
    %   Detailed explanation goes here

    psi = -vec_current(4);

    R = [cos(psi) , -sin(psi) , 0, 0;
         sin(psi) , cos(psi)   , 0, 0;
         0        ,    0        , 1, 0;
         0        ,    0        , 0, 1];
     %Roation matrix to transform the N E D to the x-y-z- coordinates

     out = R*vec(1:4);
end

