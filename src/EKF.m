classdef EKF < handle

    properties
        x_
        x = 'not_started';
        P_
        P = 'not_started';
        H
        Q
        R
        ffun
        phifun
    end

    methods

        function obj = addMeasurement(obj, z, u)
            % Checking if this is the first run
            if strcmp(obj.x, 'not_started')
                obj.x = obj.x_;
                obj.P = obj.P_;
            end


            x_ = obj.x_;
            x  = obj.x;
            P_ = obj.P_;
            P  = obj.P;
            H = obj.H;
            R = obj.R;
            I = eye(length(x_));
            Q = obj.Q;
            x = obj.x;

            % Projecting ahead from last point with the u for the current
            % iteration.
            A = obj.phifun(x);
            x_ = obj.ffun(x, u);
            P_ = A * P * A' + Q;

            % Compute Kalman Gain
            K = P_*H'/(H*P_*H'+ R);

            % Update estimate with measurement z_k
            x = x_ + K * (z - H * x_);

            % Compute error covariance for updated estimate
            ikh = I - K * H;
            P = ikh * P_ * ikh' + K * R * K';

            % Make sure P is symmetric
            P = (P + P') / 2;

            % Saving the state
            obj.x_ = x_;
            obj.P_ = P_;
            obj.x = x;
            obj.P = P;
        end

        function out = get(obj)
            out = obj.x;
        end
    end

end

